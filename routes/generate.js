const express = require('express');
const router = express.Router();
const createReport = require('docx-templates');
const fs = require('fs');

/* GET users listing. */
router.get('/', async (req, res, next) => {

  try {
    await populateDocx();
    res.render('generated');

  } catch (e) {
    console.log(e);
  }
});


const populateDocx = async () => {
  try {
    const template = fs.readFileSync('./public/templates/widigital-cv-template.docx');

    const data = {
      person: {
        name: 'JRIDI Hamza',
        role: 'Ingénieur études & développement',
        expertise: 'Full Stack Angular 2+ / Node.js',
        experience: '+4 ans',
        imageUrl: './public/images/image.png',
        diplomas: [
          {
            name: 'Concours national d\'Entrée aux cycles de Formation d\'Ingénieurs',
            school: 'Concours national d\'Entrée aux cycles de Formation d\'Ingénieurs',
            year: '2015',
          },
          {
            name: 'Diplôme national d’ingénieur informatique',
            school: 'Ecole Nationale d’Electronique et de Télécommunication',
            year: '2012',
          }
        ],
        technicalSkills: [
          { category: 'Frontend',
            technologies: [
              {
                skillName: 'AngularJS',
                level: '4',
              },
              {
                skillName: 'angular 2+',
                level: '4',
              },
              {
                skillName: 'JavaScript',
                level: '4',
              },
              {
                skillName: 'HTML5/CSS3',
                level: '3',
              }
            ]
          },
          { category: 'Backend',
            technologies: [
              {
                skillName: 'node.js',
                level: '4',
              },
              {
                skillName: 'Express',
                level: '4',
              },
              {
                skillName: 'REST APIs',
                level: '4',
              }
            ]
          }
        ]
      }
    };

    const file = await createReport({
      template,
      output: 'buffer',
      data,
    });
    fs.writeFileSync('./public/CVs/auto-generated.docx', file)

  } catch (e) {
    return console.log(e);
  }
};

module.exports = router;
